import React, { Component } from 'react';
import logo from './logo.svg';
import './Home.css';

class Home extends Component {
    render() {
        return (
            <div className="App">
                <todo-nav-bar></todo-nav-bar>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <to-do></to-do>
            </div>
        );
    }
}

export default Home;
