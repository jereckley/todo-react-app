import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import {
    Router,
    Route
} from 'react-router-dom';
import { createBrowserHistory } from 'history';

export const basename = '/todoreact';
export const history = createBrowserHistory({ basename });


class App extends Component {
  render() {
    return (
        <Router history={history}>
            <Route exact path="/" component={Home}/>
        </Router>
    )
  }
}

export default App;
