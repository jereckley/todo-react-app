import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {defineCustomElements} from 'liberty-test-todo/dist/esm/index.js'
import {defineCustomElements as df} from 'liberty-todo-test-nav/dist/esm/index.js'

ReactDOM.render(<App />, document.getElementById('root'));
//registerServiceWorker();

defineCustomElements(window)
df(window)
